import logging
import sys
from typing import Callable

import discord

from . import commands
from .config import parse_cli_args, program_args

GetStateFunc = Callable[[discord.Guild], commands.State]

_logger = logging.getLogger(__name__)


async def _root_command_error_handler(
    interaction: discord.Interaction, error: discord.app_commands.AppCommandError
):
    if interaction.response.is_done():
        respond = interaction.followup.send
    else:
        respond = interaction.response.send_message

    match error:
        case discord.app_commands.MissingRole(missing_role=role):
            _logger.info(
                "%s(id: %d) did not have the required role '%s' to execute '%s'",
                interaction.user.name,
                interaction.user.id,
                role,
                interaction.command.name or "<UNKNOWN>"
                if interaction.command is not None
                else "<UNKNOWN>",
            )
            await respond(
                f"You do not have the required role: {role}",
                ephemeral=True,
            )
        case _:
            _logger.error(
                "an error occurred during app command handling", exc_info=error
            )
            await respond("Error executing command", ephemeral=True)


def start_bot() -> int:
    discord.opus.load_opus("libopus.so.0")

    client = discord.Client(intents=discord.Intents(guilds=True, voice_states=True))

    states: dict[discord.Guild, commands.State] = dict()

    def get_state(guild: discord.Guild):
        # NOTE: `dict.setdefault` can't be used here because then
        # `commands.State` will spin up another unnecessary `asyncio.Task` for
        # the guild.
        try:
            return states[guild]
        except KeyError:
            states[guild] = commands.State(guild)
            return states[guild]

    command_tree = discord.app_commands.CommandTree(client)
    command_tree.error(_root_command_error_handler)
    command_tree.add_command(commands.queue.create_command_group(get_state))

    @client.event
    async def on_ready():
        _logger.info("bot is ready")
        synced_commands = await command_tree.sync()
        _logger.debug("global synced commands: %s", synced_commands)

    # Disabling discord log_handler because we have our own configuration
    client.run(program_args.bot_token, log_handler=None)

    return 0


def main():
    parse_cli_args()
    sys.exit(start_bot())
