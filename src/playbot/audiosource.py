import asyncio

import discord
import yt_dlp


class YTDLSource(discord.PCMVolumeTransformer):
    ytdl = yt_dlp.YoutubeDL(
        {
            "format": "bestaudio/best",
            "outtmpl": "%(extractor)s-%(id)s-%(title)s.%(ext)s",
            "restrictfilenames": True,
            "noplaylist": True,
            "nocheckcertificate": True,
            "ignoreerrors": False,
            "logtostderr": False,
            "quiet": True,
            "no_warnings": True,
            "default_search": "auto",
            "source_address": "0.0.0.0",  # bind to ipv4 since ipv6 addresses cause issues sometimes
        }
    )

    def __init__(self, source, *, data, volume=0.5):
        super().__init__(source, volume)
        self.data = data
        self.title = data.get("title")
        self.url = data.get("url")

    def __str__(self):
        return str(self.title)

    @classmethod
    async def from_url(cls, url, *, loop=None, stream=True):
        loop = loop or asyncio.get_running_loop()
        data = await loop.run_in_executor(
            None, lambda: cls.ytdl.extract_info(url, download=not stream)
        )
        assert data is not None

        if "entries" in data:
            # take first item from a playlist
            data = data["entries"][0]

        source = data["url"] if stream else cls.ytdl.prepare_filename(data)
        opts = (
            "-reconnect 1",
            "-reconnect_at_eof 1",
            "-reconnect_streamed 1",
            "-reconnect_delay_max 2",
        )
        return cls(
            discord.FFmpegPCMAudio(
                source, before_options=" ".join(opts), options="-vn"
            ),
            data=data,
        )
