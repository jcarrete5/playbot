"""Program argument management utilites."""

import os
from argparse import ArgumentParser
from collections.abc import Sequence

from .config import Config


class ArgsNamespace:
    """Program argument definitions.

    This class serves as a namespace for command-line arguments and relevant
    environment variables.
    """

    def __init__(self):
        self._config: Config
        self._idle_disconnect_timeout: float | None
        self._bot_token: str

    @property
    def logging_dict(self):
        """Logging configuration dictionary.

        This dictionary should be passed to ``logging.config.dictConfig``.
        """
        return self._config.logging

    @property
    def idle_disconnect_timeout(self):
        """Time in seconds to wait while idling before disconnecting."""
        if self._idle_disconnect_timeout is None:
            return self._config.idle_disconnect_timeout
        else:
            return self._idle_disconnect_timeout

    @property
    def bot_token(self):
        """Discord bot token needed to use the discord API."""
        return os.environ["BOT_TOKEN"]


program_args = ArgsNamespace()


def parse_cli_args(args: Sequence[str] | None = None):
    """Parse command-line arguments.

    The parsed arguments are stored in ``program_args``.
    """
    parser = ArgumentParser(
        epilog=(
            "Arguments specified on the command line will override those "
            "in the configuration file."
        )
    )
    parser.add_argument(
        "-c",
        "--config",
        help="specify the path to the bot configuration",
        type=Config,
        default="playbot.toml",
        dest="_config",
        metavar="CONFIG_PATH",
    )
    parser.add_argument(
        "-t",
        "--timeout",
        help="time in seconds to wait while idling before disconnecting",
        type=float,
        dest="_idle_disconnect_timeout",
        metavar="TIME_IN_SECONDS",
    )
    parser.parse_args(args, namespace=program_args)
