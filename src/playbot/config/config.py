import logging
import logging.config
import tomllib
from pathlib import Path
from typing import Any

_logger = logging.getLogger(__name__)


class Config:
    """Configuration file interface.

    When a breaking change is made to the configuration file schema, the version
    number should be incremented. This indicates that the existing configuation
    must to updated to the new schema in order to be used.
    """

    def __init__(self, path_str: str):
        self.path_str = path_str
        self._load()

    def _load(self):
        with Path(self.path_str).open("rb") as infile:
            self._config = tomllib.load(infile)
        logging.config.dictConfig(self.logging)

    def reload(self):
        old_config = self._config
        _logger.info("reloading config from '%s'", Path(self.path_str).resolve())
        try:
            self._load()
        except tomllib.TOMLDecodeError:
            self._config = old_config
            _logger.warning(
                "failed to reload config and will continue using the old config"
            )
            raise

    @property
    def logging(self) -> dict[str, Any]:
        """Logging configuration dictionary.

        Should be passed to ``logging.config.dictConfig``."""
        return self._config["logging"]

    @property
    def idle_disconnect_timeout(self) -> float:
        """Time in seconds the bot can be idle before disconnecting."""
        return self._config.get("idle-disconnect-timeout", 120)
