import asyncio
import logging
from dataclasses import dataclass
from typing import Callable, Literal

import discord

from playbot.config import program_args

MaybeVoiceClient = Callable[[], discord.VoiceClient | None]
VoiceGuildChannel = discord.VoiceChannel | discord.StageChannel
Result = tuple[Literal[True], None] | tuple[Literal[False], str]

_logger = logging.getLogger(__name__)


@dataclass
class _AudioItem:
    source: discord.AudioSource
    """Audio source to play."""

    voice_channel: VoiceGuildChannel
    """Voice channel to play the audio source."""

    def title(self) -> str:
        return str(self.source)


class _AudioQueue:
    _cls_logger = _logger.getChild("_AudioQueue")

    def __init__(self, guild_name: str | None = None, *, start_task: bool = True):
        if guild_name is None:
            name = f"unknown-{id(self):x}"
        else:
            name = guild_name.replace(" ", "-").lower()

        self._voice_client: discord.VoiceClient | None = None
        self._queue: list[_AudioItem] = []
        """Queue holding the sequence of audio to be played.

        The 0th position is the front of the queue, i.e. the next audio item to
        be played, and the last position is the back of the queue.
        """
        self._queue_task: asyncio.Task[None] | None = None
        self._new_audio_item = asyncio.Event()
        self._playback_ended = asyncio.Event()
        self._playback_ended.set()
        self._logger = logging.LoggerAdapter(_AudioQueue._cls_logger, {"guild": name})

        if start_task:
            self.start_task(name)

    async def _next_audio_item(self):
        """Get the next audio in the queue.

        If no audio is in the queue, wait until audio is put on the
        queue or timeout.

        :raises asyncio.TimeoutError: if no audio arrives within a defined interval
        """
        if len(self._queue) == 0:
            await asyncio.wait_for(
                self._new_audio_item.wait(), program_args.idle_disconnect_timeout
            )
        audio_item = self._queue.pop(0)
        self._new_audio_item.clear()
        return audio_item

    def _finalize_playback(
        self, exception: Exception | None, loop: asyncio.AbstractEventLoop
    ):
        if exception is not None:
            self._logger.error("error during audio playback", exc_info=exception)

        def callback():
            self._playback_ended.set()
            self._logger.info("playback completed")

        loop.call_soon_threadsafe(callback)

    async def _get_voice_client(self, channel: VoiceGuildChannel):
        if self._voice_client is None:
            self._voice_client = await channel.connect(self_deaf=True)
        else:
            need_to_move = self._voice_client.channel != channel
            if need_to_move:
                await self._voice_client.move_to(channel)
        return self._voice_client

    async def _loop(self):
        async def run_logic():
            await self._playback_ended.wait()
            audio_item = await self._next_audio_item()
            voice_client = await self._get_voice_client(audio_item.voice_channel)

            try:
                loop = asyncio.get_running_loop()
                voice_client.play(
                    audio_item.source,
                    after=lambda exc: self._finalize_playback(exc, loop),
                )
                self._playback_ended.clear()
            except discord.ClientException:
                self._logger.exception("error starting audio playback")

        while True:
            try:
                await run_logic()
            except asyncio.TimeoutError:
                if self._voice_client is not None and self._voice_client.is_connected():
                    self._logger.info("disconnecting from voice")
                    await self._voice_client.disconnect()
                    self._voice_client = None
            except asyncio.CancelledError:
                self._logger.info("cancelled")
                raise

    def __len__(self):
        return len(self._queue)

    def __iter__(self):
        return iter(self._queue)

    def __del__(self):
        self._cancel_task()

    def _cancel_task(self):
        if self._queue_task is not None:
            try:
                self._queue_task.cancel()
            except RuntimeError:
                pass  # Ignore failure to cancel due to event loop being closed

    def start_task(self, name: str):
        if self._queue_task is None:
            self._queue_task = asyncio.create_task(
                self._loop(),
                name=f"{name}-audio-queue-task",
            )
        else:
            self._logger.warning("task is already started; not starting another task")

    def put(self, source: discord.AudioSource, from_channel: VoiceGuildChannel):
        """Insert an item into the back of the queue.

        :param source: audio source to play next
        :param from_channel: the voice channel the command was executed from
        """
        self._queue.append(_AudioItem(source, from_channel))
        self._new_audio_item.set()
        self._logger.info("pushed %s onto queue", source)
        self._logger.debug(
            "queue is %s", tuple(str(item.source) for item in self._queue)
        )

    def skip(self) -> Result:
        if self._voice_client is None or not self._voice_client.is_connected():
            return (False, "client is not connected")

        if not self._voice_client.is_playing():
            return (False, "nothing is playing to skip")

        self._logger.info("skipping %r", self._voice_client.source)
        self._voice_client.stop()

        return (True, None)

    def move(self, src: int, dst: int | None = None) -> Result:
        """Move an item from ``src`` to ``dst`` in the queue.

        Preconditions:
        - ``0 <= src < len(self)``
        - ``0 <= dst < len(self)``

        If ``dst`` is not specified, move ``src`` to the front of the queue.

        :param src: source position in the queue
        :param dst: destination position in the queue
        """
        dst = dst if dst is not None else 0
        assert 0 <= src < len(self._queue)
        assert 0 <= dst < len(self._queue)

        item = self._queue.pop(src)
        self._queue.insert(dst, item)

        self._logger.debug(
            "queue is %s", tuple(str(item.source) for item in self._queue)
        )

        return (True, None)


class State:
    def __init__(self, guild: discord.Guild):
        self.guild = guild
        self.queue = _AudioQueue(guild.name)
