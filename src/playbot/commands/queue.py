"""Implements commands in the "queue" command group."""

from __future__ import annotations

import itertools
import logging
from dataclasses import dataclass
from typing import TYPE_CHECKING

import discord

from playbot import audiosource

from .checks import is_guild_interaction, is_guild_member
from .views import Page, PageNavigator

if TYPE_CHECKING:
    from playbot.bot import GetStateFunc

_logger = logging.getLogger(__name__)


def create_command_group(get_state: GetStateFunc) -> discord.app_commands.Group:
    group = discord.app_commands.Group(
        name="queue", description="Manage audio queue", guild_only=True
    )

    @group.command()
    @discord.app_commands.guild_only
    @discord.app_commands.check(is_guild_interaction)
    @discord.app_commands.check(is_guild_member)
    async def play(
        interaction: discord.Interaction,
        attachment: discord.message.Attachment | None,
        url: str | None,
    ):
        """Put something on the queue"""
        _logger.info("%s executed '/queue play'", interaction.user)
        _logger.debug("%s", f"{attachment=} {url=}")

        await interaction.response.defer(thinking=True)

        assert interaction.guild is not None
        state = get_state(interaction.guild)

        @dataclass
        class SourceInfo:
            url: str
            audio_source: discord.AudioSource

        match (attachment, url):
            case discord.message.Attachment(url=source_url), None:
                source_info = SourceInfo(
                    source_url,
                    await discord.FFmpegOpusAudio.from_probe(source_url),
                )
            case None, str() as source_url:
                source_info = SourceInfo(
                    source_url,
                    await audiosource.YTDLSource.from_url(source_url),
                )
            case None, None:
                await interaction.followup.send("No argument provided.", ephemeral=True)
                return
            case _:
                await interaction.followup.send(
                    "Too many arguments provided.", ephemeral=True
                )
                return

        assert isinstance(interaction.user, discord.Member)
        voice_state = interaction.user.voice
        if voice_state is None or voice_state.channel is None:
            await interaction.followup.send(
                "You must be in a voice channel to use this command.", ephemeral=True
            )
            return

        state.queue.put(source_info.audio_source, voice_state.channel)
        await interaction.followup.send(f"Added audio to the queue: {source_info.url}")

    @group.command()
    @discord.app_commands.guild_only
    @discord.app_commands.check(is_guild_interaction)
    @discord.app_commands.check(is_guild_member)
    async def skip(interaction: discord.Interaction):
        """Skip the currently playing audio"""
        _logger.info("%s executed '/queue skip'", interaction.user)

        await interaction.response.defer(thinking=True)

        assert interaction.guild is not None
        state = get_state(interaction.guild)

        match state.queue.skip():
            case True, None:
                msg = "Skipping the current audio..."
            case False, str() as error_msg:
                msg = error_msg.capitalize()

        await interaction.followup.send(msg)

    @group.command()
    @discord.app_commands.guild_only
    @discord.app_commands.check(is_guild_interaction)
    @discord.app_commands.check(is_guild_member)
    async def move(
        interaction: discord.Interaction, source: int, destination: int | None
    ):
        """Move audio from source to destination or the start if not specified"""
        _logger.info(
            "%s executed '/queue move %r %r'", interaction.user, source, destination
        )

        await interaction.response.defer(thinking=True)

        assert interaction.guild is not None
        state = get_state(interaction.guild)

        if not 1 <= source <= len(state.queue):
            await interaction.followup.send("Source position is out of range")
            return

        if destination is None:
            destination = 1
        if not 1 <= destination <= len(state.queue):
            await interaction.followup.send("Destination position is out of range")
            return

        state.queue.move(source - 1, destination - 1)

        await interaction.followup.send(
            f"Moved item from position {source} to "
            f"{destination if destination != 1 else 'the front of the queue'}"
        )

    @group.command()
    @discord.app_commands.guild_only
    @discord.app_commands.check(is_guild_interaction)
    @discord.app_commands.check(is_guild_member)
    async def list(interaction: discord.Interaction):
        """List items in the audio queue"""
        _logger.info("%s executed '/queue list'", interaction.user)

        assert interaction.guild is not None
        state = get_state(interaction.guild)

        if len(state.queue) == 0:
            await interaction.response.send_message("Queue is empty")
            return

        results_per_page = 10

        def make_page(page_index: int) -> Page:
            pages = tuple(itertools.batched(state.queue, n=results_per_page))
            page = pages[page_index]
            offset = page_index * results_per_page
            return {
                "embeds": [
                    discord.Embed(title=f"{i + offset + 1}. {item.title()}")
                    for i, item in enumerate(page)
                ]
            }

        navigator = PageNavigator(
            interaction.response,
            make_page,
            total_pages=PageNavigator.total_pages(results_per_page, len(state.queue)),
        )
        await navigator.send()

    return group
