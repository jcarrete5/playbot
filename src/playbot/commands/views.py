import math
from collections.abc import Mapping
from typing import Callable

import discord

Page = Mapping
PageNum = int
PageMaker = Callable[[PageNum], Page]
Response = discord.InteractionResponse


class PageNavigator(discord.ui.View):
    def __init__(
        self,
        response: Response,
        make_page: PageMaker,
        *,
        total_pages: int | None = None,
        timeout=30,
    ):
        super().__init__(timeout=timeout)
        self._response = response
        self._make_page = make_page
        self._page_index = 0
        self._total_pages = total_pages

    async def send(self, page_index: int = 0):
        self._page_index = page_index
        self._update_buttons()
        kwargs = self._make_page(page_index)
        await self._response.send_message(
            **kwargs, view=self, content=self._add_page_num(kwargs.get("content"))
        )

    async def _update(self, interaction: discord.Interaction):
        self._update_buttons()
        kwargs = self._make_page(self._page_index)
        await interaction.response.edit_message(
            **kwargs, view=self, content=self._add_page_num(kwargs.get("content"))
        )

    def _add_page_num(self, content: str | None):
        if self._total_pages is None:
            page_num = f"Page {self._page_index + 1}"
        else:
            page_num = f"Page {self._page_index + 1} of {self._total_pages}"
        return "\n".join((content, page_num)) if content else page_num

    def _update_buttons(self):
        first_btn, prev_btn, next_btn, last_btn = self.children
        assert isinstance(first_btn, discord.ui.Button)
        assert isinstance(prev_btn, discord.ui.Button)
        assert isinstance(next_btn, discord.ui.Button)
        assert isinstance(last_btn, discord.ui.Button)
        assert self._page_index >= 0

        if self._page_index == 0:
            first_btn.disabled = True
            prev_btn.disabled = True
        else:
            first_btn.disabled = False
            prev_btn.disabled = False

        if self._total_pages is None:
            next_btn.disabled = False
            last_btn.disabled = True
        elif self._page_index + 1 < self._total_pages:
            next_btn.disabled = False
            last_btn.disabled = False
        else:
            next_btn.disabled = True
            last_btn.disabled = True

    @discord.ui.button(label="⏪")
    async def first(self, interaction: discord.Interaction, button: discord.ui.Button):
        self._page_index = 0
        await self._update(interaction)

    @discord.ui.button(label="◀️")
    async def prev(self, interaction: discord.Interaction, button: discord.ui.Button):
        self._page_index -= 1
        assert self._page_index >= 0
        await self._update(interaction)

    @discord.ui.button(label="▶️")
    async def next(self, interaction: discord.Interaction, button: discord.ui.Button):
        self._page_index += 1
        assert self._total_pages is None or self._page_index < self._total_pages
        await self._update(interaction)

    @discord.ui.button(label="⏩")
    async def last(self, interaction: discord.Interaction, button: discord.ui.Button):
        assert self._total_pages is not None
        self._page_index = self._total_pages - 1
        await self._update(interaction)

    @staticmethod
    def total_pages(results_per_page: int, total_results: int) -> int:
        return math.ceil(total_results / results_per_page)
