import discord


def is_guild_interaction(interaction: discord.Interaction) -> bool:
    """Check that command originated from a guild."""
    return interaction.guild is not None


def is_guild_member(interaction: discord.Interaction) -> bool:
    """Check that command originated from a guild member."""
    return isinstance(interaction.user, discord.Member)
