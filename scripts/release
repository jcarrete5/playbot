#!/usr/bin/env bash
#
# Bump and tag a new release.
#
# Increment the version stored in `.VERSION` based on the given argument:
#   major: Increment the first number and reset the second and third number
#          to 0.
#   minor: Increment the second number and reset the third number to 0.
#   patch: Increment the third number.
# 
# Then add and commit `.VERSION` and tag the commit. The CI/CD pipeline will
# handle building and deploying the release.

set -e

script_dir=$(cd -- "$(dirname -- "${BASH_SOURCE[0]}")" &> /dev/null && pwd)
cd "$script_dir/.."

revision="$1"

# Bump version
old_version="$(cat .VERSION)"
new_version="$(scripts/bump_version "$old_version" "$revision")"
tag_name="v${new_version}"
echo "$old_version -> $new_version"

# Write new version
echo "$new_version" > .VERSION
git add .VERSION

# Update CHANGELOG.md
"$EDITOR" CHANGELOG.md
git add CHANGELOG.md

# Commit and push
git commit -m "Release $tag_name"
git tag -s "$tag_name"
echo "Release changes committed!"
echo "  Run 'git push origin $tag_name' to release the new version"

