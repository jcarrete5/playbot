# Playbot

A Discord bot to play audio through discord.

Commands are implemented using discord's app commands API. Type "/" in a
text channel to see what playbot can do.

## Features

- Play audio from attachments or URLs in a discord voice channel
- Queue multiple audio clips to be played in sequence

## Hosting Playbot

Playbot is not currently hosted anywhere and therefore, cannot simply be invited
to a discord server. If you would like to use Playbot, you must host the bot
yourself. This project is designed to be built as a Docker image and run in a
container. To build the image, simply run

```shell
docker build -t myplaybot:latest .
```

in the project root.

Then head over to Discord's developer portal and create a
[discord application](https://discord.com/developers/applications). This will
provide you with the necessary bot token to run Playbot.

To start the bot, run

```shell
docker run -v $PWD:/config:ro -e BOT_TOKEN=<your-bot-token> myplaybot:latest
```

Once the bot is running, you'll need to invite the bot to a server. You can
[create an invite link][discord-invite-generator] and use it to invite the bot
to a server. After the bot is added to the server, you can use commands by
starting a message with a '/'.

## Contributing

To contribute, code or issues, see [CONTRIBUTING.md](CONTRIBUTING.md).

[discord application]: https://discord.com/developers/applications
[discord-invite-generator]: https://discordapi.com/permissions.html#3145728
