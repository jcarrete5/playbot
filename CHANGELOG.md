# Changelog

## *Unreleased*

## v0.3.2 - 2025-01-29

### Changed

- Upgrade yt-dlp dependency. Solves [#14](https://gitlab.com/jcarrete5/playbot/-/issues/14)

## v0.3.1 - 2024-04-29

### Changed

- Upgrade dependencies

## v0.3.0 - 2023-11-30

### Added

- Audio queue reordering: use `/queue move` to reorder items in the queue.
- Audio queue listing: use `/queue list` to list the items in the the.

### Fixed

- An issue where playing audio would end a few seconds too early.

## v0.2.0 - 2023-11-21

### Added

- Audio queuing: repeated uses of `/queue play` will queue audio to be played
  next.
- Audio skipping: use `/queue skip` to skip the currently playing audio.

### Changed

- `/play` app command now lives under `/queue play`.

### Fixed

- An issue that would cause multiple audio queues to be created for a guild.
- Prevent multiple arguments being passed to `/queue play` at once.

## v0.1.0 - 2023-11-13

### Added

- app command `/play` to play audio from an uploaded attachement or a URL. Tested
  with youtube and soundcloud, however, the command should work with any URL
  supported by [youtube-dl][].
- Auto-disconnect after bot hasn't played anything for a while.

[youtube-dl]: https://github.com/ytdl-org/youtube-dl
