# Contributing

Playbot is open to contributions. If there is a feature you want to see or a
bug that needs fixing, create an issue in the [issue tracker on
GitLab][issue-tracker].

## Development Environment

Create a virtual environment and install the project in editable mode.

```shell
python -m venv --upgrade-deps venv/  # Create a virtual environment
. venv/bin/activate                  # Activate virtual environment
pip install -U pip-tools             # Install pip-tools
pip-sync requirements-dev.txt        # Install playbot dev dependencies
pip install -e .                     # Install playbot in editable mode
```

After executing the above commands, you should be able to start playbot. You
can test you set everything up correctly if `run-playbot -h` shows the help
text for playbot. It should look something like:

    usage: run-playbot [-h] [-c CONFIG_PATH] [-t TIME_IN_SECONDS]

    options:
    -h, --help            show this help message and exit
    -c CONFIG_PATH, --config CONFIG_PATH
    specify the path to the bot configuration
    -t TIME_IN_SECONDS, --timeout TIME_IN_SECONDS
    time in seconds to wait while idling before disconnecting

    Arguments specified on the command line will override those in the configuration file.

The rest of this guide assumes you have activated the python virtual
environment already.

### Running the application locally

In order to run the application, you must create a [discord application][] and
receive a bot token. Then set the environment variable `BOT_TOKEN` to this value
and run `run-playbot`.

```shell
BOT_TOKEN='EXAMPLE_TOKEN' run-playbot
```

You'll also have to add the bot to a server to see it do anything. You can
[create an invite link][discord-invite-generator] and paste the created URL into
your browser to invite the bot to a server.

[issue-tracker]: https://gitlab.com/jcarrete5/playbot/-/issues
[discord application]: https://discord.com/developers/applications
[discord-invite-generator]: https://discordapi.com/permissions.html#3145728
