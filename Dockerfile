# NOTE: This image searches for 'playbot.toml' in the '/config' directory to
# configure the application. Bind-mount a directory containing the config file
# to '/config' when running a container using this image.

FROM python:3.12.3-alpine AS build

RUN apk update && apk add opus ffmpeg git gcc musl-dev

WORKDIR /app

# Setup virtual environment
ENV VIRTUAL_ENV=/app/venv
RUN python -m venv --upgrade-deps "$VIRTUAL_ENV"
ENV PATH="$VIRTUAL_ENV/bin:$PATH"
RUN pip install --no-cache-dir -U pip-tools

# First cache dependencies in docker layer
COPY requirements.txt .
RUN pip-sync --pip-args "--no-cache-dir"

# Then include and build the project source
COPY pyproject.toml src/ README.md LICENSE .VERSION .
RUN pip install --no-cache-dir .

RUN pip uninstall -y --no-cache-dir pip-tools


FROM python:3.12.3-alpine
RUN apk update && apk add opus ffmpeg

# Create a user and group to run the application
RUN addgroup -S playbot && adduser -S -g playbot playbot

WORKDIR /app

ENV VIRTUAL_ENV=/app/venv
ENV PATH="$VIRTUAL_ENV/bin:$PATH"

COPY --from=build /app/venv /app/venv

ENV PYTHONDONTWRITEBYTECODE=1 PYTHONUNBUFFERED=1
USER playbot:playbot
ENTRYPOINT ["run-playbot", "--config=/config/playbot.toml"]
