import pytest

from playbot.config import parse_cli_args


@pytest.fixture(autouse=True)
def set_program_args():
    parse_cli_args([])


@pytest.fixture
def reduced_idle_timeout():
    parse_cli_args(["--timeout", "1"])
