import asyncio
from math import exp
from unittest.mock import AsyncMock

import discord
import pytest

from playbot.commands.state import _AudioItem, _AudioQueue

sample_audio_queue = tuple(
    _AudioItem(AsyncMock(discord.AudioSource), AsyncMock(discord.VoiceChannel))
    for _ in range(10)
)


@pytest.fixture
def audio_queue():
    return _AudioQueue(start_task=False)


@pytest.fixture
async def start_audio_queue_task(audio_queue: _AudioQueue):
    audio_queue.start_task("test")
    yield

    # Cancel the queue task here to avoid RuntimeWarnings about coroutines not
    # being awaited. I'm not 100% sure as to why the coroutines are not
    # awaited. My current theory is that the event loop is stopped before they
    # have a chance to be awaited which causes the warning.
    audio_queue._cancel_task()


@pytest.fixture
def make_voice_channel_mock():
    def make(*args, **kwargs):
        channel = AsyncMock(discord.VoiceChannel, *args, **kwargs)
        channel.configure_mock(
            **{"connect.return_value": AsyncMock(discord.VoiceClient)}
        )
        return channel

    return make


@pytest.fixture
def init_voice_client(audio_queue: _AudioQueue):
    voice_client = AsyncMock(discord.VoiceClient)
    audio_queue._voice_client = voice_client
    return voice_client


@pytest.fixture
def init_voice_channel(init_voice_client: AsyncMock, make_voice_channel_mock):
    channel = make_voice_channel_mock()
    init_voice_client.configure_mock(channel=channel)
    return channel


@pytest.fixture
def init_audio_queue_items(audio_queue):
    items = list(sample_audio_queue)
    audio_queue._queue = items
    return items


@pytest.mark.usefixtures("reduced_idle_timeout")
async def test_bot_disconnects_after_not_getting_new_audio_after_a_timeout(
    audio_queue: _AudioQueue, init_voice_client: AsyncMock, start_audio_queue_task
):
    del start_audio_queue_task
    init_voice_client.configure_mock(**{"is_connected.return_value": True})

    # Allow audio queue to timeout
    await asyncio.sleep(2)

    assert audio_queue._voice_client is None


async def test_bot_joins_voice_channel(
    audio_queue: _AudioQueue, make_voice_channel_mock, start_audio_queue_task
):
    del start_audio_queue_task
    channel = make_voice_channel_mock()
    source = AsyncMock(discord.AudioSource)
    voice_client = channel.connect.return_value
    audio_queue._queue.append(_AudioItem(source, channel))

    # Signal new audio item
    audio_queue._new_audio_item.set()

    # Allow audio queue to run for a bit
    await asyncio.sleep(0.2)

    assert audio_queue._voice_client is voice_client
    channel.connect.assert_called_once_with(self_deaf=True)


@pytest.mark.usefixtures("init_voice_channel")
async def test_bot_switches_to_correct_channel_to_play_audio(
    audio_queue: _AudioQueue, make_voice_channel_mock, start_audio_queue_task
):
    del start_audio_queue_task
    source = AsyncMock(discord.AudioSource)
    new_channel = make_voice_channel_mock()
    audio_queue._queue.append(_AudioItem(source, new_channel))

    # Signal new audio item
    audio_queue._new_audio_item.set()

    # Allow audio queue to run for a bit
    await asyncio.sleep(0.2)

    assert audio_queue._voice_client is not None
    audio_queue._voice_client.move_to.assert_called_once_with(new_channel)


async def test_bot_stays_in_correct_channel_to_play_audio(
    audio_queue: _AudioQueue, init_voice_channel: AsyncMock, start_audio_queue_task
):
    del start_audio_queue_task
    source = AsyncMock(discord.AudioSource)
    audio_queue._queue.append(_AudioItem(source, init_voice_channel))

    # Signal new audio item
    audio_queue._new_audio_item.set()

    # Allow audio queue to run for a bit
    await asyncio.sleep(0.2)

    assert audio_queue._voice_client is not None
    assert audio_queue._voice_client.channel is init_voice_channel


async def test_queued_audio_plays(
    audio_queue: _AudioQueue, init_voice_channel: AsyncMock, start_audio_queue_task
):
    del start_audio_queue_task
    source = AsyncMock(discord.AudioSource)
    audio_queue._queue.append(_AudioItem(source, init_voice_channel))

    # Signal new audio item
    audio_queue._new_audio_item.set()

    # Allow audio queue to run for a bit
    await asyncio.sleep(0.2)

    assert audio_queue._voice_client is not None
    assert audio_queue._voice_client.play.call_args[0][0] is source


async def test_queued_audio_plays_in_sequence(
    audio_queue: _AudioQueue, init_voice_channel: AsyncMock, start_audio_queue_task
):
    del start_audio_queue_task
    source = AsyncMock(discord.AudioSource)
    source2 = AsyncMock(discord.AudioSource)
    audio_queue._queue.append(_AudioItem(source, init_voice_channel))
    audio_queue._queue.append(_AudioItem(source2, init_voice_channel))

    # Simulate first audio completing playback
    audio_queue._new_audio_item.set()
    await asyncio.sleep(0.5)
    audio_queue._playback_ended.set()

    # Simulate second audio completing playback
    audio_queue._new_audio_item.set()
    await asyncio.sleep(0.5)
    audio_queue._playback_ended.set()

    assert audio_queue._voice_client is not None
    assert audio_queue._voice_client.play.call_args_list[0][0][0] is source
    assert audio_queue._voice_client.play.call_args_list[1][0][0] is source2


async def test_queue_is_iterable_in_order(audio_queue: _AudioQueue):
    expected_result = [
        _AudioItem(AsyncMock(discord.AudioSource), AsyncMock(discord.VoiceChannel))
        for _ in range(10)
    ]

    audio_queue._queue = expected_result

    assert list(audio_queue) == expected_result


class TestPut:
    def test_inserts_items_to_back_of_queue(self, audio_queue: _AudioQueue):
        audio_source = AsyncMock(discord.AudioSource)
        audio_source2 = AsyncMock(discord.AudioSource)
        voice_channel = AsyncMock(discord.VoiceChannel)

        audio_queue.put(audio_source, voice_channel)
        audio_queue.put(audio_source2, voice_channel)

        assert audio_queue._queue[0].source is audio_source
        assert audio_queue._queue[0].voice_channel is voice_channel
        assert audio_queue._queue[1].source is audio_source2
        assert audio_queue._queue[1].voice_channel is voice_channel


class TestSkip:
    def test_skips_playing_audio(
        self, audio_queue: _AudioQueue, init_voice_client: AsyncMock
    ):
        init_voice_client.configure_mock(
            **{
                "is_connected.return_value": True,
                "is_playing.return_value": True,
            }
        )

        (ok, _) = audio_queue.skip()

        assert ok is True
        init_voice_client.stop.assert_called_once()

    def test_returns_error_when_client_is_not_connected(
        self, audio_queue: _AudioQueue, init_voice_client: AsyncMock
    ):
        init_voice_client.configure_mock(
            **{
                "is_connected.return_value": False,
                "is_playing.return_value": True,
            }
        )

        (ok, _) = audio_queue.skip()

        assert ok is False

    def test_returns_error_when_client_is_none(self, audio_queue: _AudioQueue):
        (ok, _) = audio_queue.skip()

        assert ok is False

    def test_returns_error_when_client_is_not_playing_audio(
        self, audio_queue: _AudioQueue, init_voice_client: AsyncMock
    ):
        init_voice_client.configure_mock(
            **{
                "is_connected.return_value": True,
                "is_playing.return_value": False,
            }
        )

        (ok, _) = audio_queue.skip()

        assert ok is False


@pytest.mark.usefixtures("init_audio_queue_items")
class TestMove:
    @pytest.mark.parametrize("src,dst", ((-1, 0), (100, 0), (-1, 0), (0, 100)))
    def test_move_raises_error_with_invalid_args(
        self, audio_queue: _AudioQueue, src: int, dst: int
    ):
        with pytest.raises(AssertionError):
            audio_queue.move(src, dst)

    @pytest.mark.parametrize(
        "src,dst,expected_result_sequence",
        (
            (9, None, [sample_audio_queue[9], *sample_audio_queue[:9]]),
            (9, 0, [sample_audio_queue[9], *sample_audio_queue[:9]]),
            (0, 9, [*sample_audio_queue[1:], sample_audio_queue[0]]),
            (5, 5, [*sample_audio_queue]),
            (
                5,
                4,
                [
                    *sample_audio_queue[:4],
                    sample_audio_queue[5],
                    sample_audio_queue[4],
                    *sample_audio_queue[6:],
                ],
            ),
            (
                0,
                3,
                [
                    *sample_audio_queue[1:4],
                    sample_audio_queue[0],
                    sample_audio_queue[4],
                    *sample_audio_queue[5:],
                ],
            ),
        ),
    )
    def test_move_can_move_item_to_back_of_queue(
        self,
        audio_queue: _AudioQueue,
        src: int,
        dst: int | None,
        expected_result_sequence: list[_AudioItem],
    ):
        audio_queue.move(src, dst)

        assert audio_queue._queue == expected_result_sequence
